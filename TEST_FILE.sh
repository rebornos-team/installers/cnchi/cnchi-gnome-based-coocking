#  Copyright © 2016-2019 RebornOS
#
#  This file is part of RebornOS.
#
#  Reborn OS is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Reborn OS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  The following additional terms are in effect as per Section 7 of the license:
#
#  The preservation of all legal notices and author attributions in
#  the material or in the Appropriate Legal Notices displayed
#  by works containing it is required.
#
#  You should have received a copy of the GNU General Public License
#  along with Reborn OS; If not, see <http://www.gnu.org/licenses/>.

﻿gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout '0' && gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout '0'
WM=`wmctrl -m | grep Name | awk '{print $2}'`
echo $WM
LINE=$(grep -o "B" <<<"$WM" | wc -l)
echo $LINE
if [[ $LINE == 1 ]]; then
dconf write /org/gnome/desktop/background/picture-uri "'file:///usr/share/backgrounds/abstract1-reborn2.png'"
dconf write /org/gnome/desktop/interface/gtk-theme "'Evo-Pop'"
dconf write /org/gnome/desktop/interface/icon-name "'Flat-Remix'"
fi
if [ $LINE -eq "0" ]; then
dconf write /org/gnome/desktop/background/picture-uri "'file:///usr/share/backgrounds/elementary.jpg'"
dconf write /org/gnome/shell/extensions/user-theme/name "'Arctic-Apricity'"
dconf write /org/gnome/desktop/interface/gtk-theme "'Arc'"
dconf write /org/gnome/desktop/interface/icon-name "'Apricity-Icons'"
fi
wget --spider www.google.com
if [ "$?" = 0 ]; then
  sudo rm -rf /etc/pacman.d/gnupg
  sudo pacman -Syy
  sudo pacman-key --init
  sudo pacman-key --populate archlinux rebornos
  sudo pacman-key --refresh-keys
  sudo pacman -Syy
  reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist
# if [ ! -z $(grep "eu" "etc/pacman.d/mirrorlist") ]; then 
# sudo cp /usr/bin/cnchi/pacman.conf /etc/
# sudo mv /usr/bin/cnchi/reborn-mirrorlist2 /etc/pacman.d/reborn-mirrorlist
# fi
else exec /usr/bin/internet.sh
fi
